superbuild_add_project_python_wheel(scipy
  DEPENDS pythonsetuptools python3 numpy
  LICENSE_FILES_WHEEL
    scipy/LICENSE.txt
    scipy/LICENSES_bundled.txt
  )
